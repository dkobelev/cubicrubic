#ifndef ARROW_H
#define ARROW_H
#include "point.h"
#include <QGLWidget>
class Arrow
{
public:
    Arrow();
    Arrow(Point * points);
    Arrow(Arrow & arrow);
    Arrow & operator=(Arrow & arrow);
    void SetArrowCoords(Point * points);
    void SetArrowCoords(float x1, float y1, float z1, float x2, float y2, float z2,float x3, float y3, float z3 );
    void SetArrowCoords(float *coords);
    Point & GetArrowCoords(int num);
    Point & GetPoint1();
    Point & GetPoint2();
    Point & GetPoint3();
    void PrintArrowCoords();
    void DrawArrow();
    bool CheckMousePointer(float x, float y);
    void ArrowRotate(float degree, float x, float y, float z);
    void SetMiddle(float x_middle,float y_middle);
    float GetMiddleX();
      float GetMiddleY();
private:
    Point point[3];
    float middle_x;
    float middle_y;
    int flag;
};

#endif // ARROW_H
