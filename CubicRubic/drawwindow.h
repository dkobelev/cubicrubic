#ifndef DRAWWINDOW_H
#define DRAWWINDOW_H
#include <QWidget>
#include <QGLWidget>
#include <QTimer>
#include <QMouseEvent>
#include <QKeyEvent>

class DrawWindow : public QGLWidget
{
    Q_OBJECT
public:
    explicit DrawWindow(QWidget *parent = 0);
    void   initializeGL();
    void  paintGL();
    void  resizeGL(int width, int height);

    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void rotate();
    QTimer & GetTime(){return timer;}
private:
    int windowWidth;
    int windowHeight;
  QTimer timer;
  double angle_x;
  double angle_y;
  double mouse_x;
  double mouse_y;

  int key;
};

#endif // DRAWWINDOW_H

