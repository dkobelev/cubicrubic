#include "arrow.h"
#include <iostream>
#include <math.h>

float Arrow::GetMiddleX()
{
    return middle_x;
}

float Arrow::GetMiddleY()
{
    return middle_y;
}

Arrow::Arrow()
{
    for(int i=0;i<3;i++)
        point[i].PointSetCoords(0,0,0);
    flag=0;
    middle_x=0;
    middle_y=0;
}
Arrow::Arrow(Point * points)
{
    flag=0;
    middle_x=0;
    middle_y=0;
    for(int i=0;i<3;i++)
        point[i]=points[i];
}
Arrow::Arrow(Arrow & arrow)
{
    for(int i=0;i<3;i++)
        point[i]=arrow.GetArrowCoords(i);
}
Arrow & Arrow::operator=(Arrow & arrow)
{
    for(int i=0;i<3;i++)
        point[i]=arrow.GetArrowCoords(i);
    return *this;
}
void Arrow::SetArrowCoords(Point * points)
{
    for(int i=0;i<3;i++)
        point[i]=points[i];
}
 Point & Arrow::GetArrowCoords(int num)
{
    return point[num];
}
 Point & Arrow::GetPoint1()
{
    return point[0];
}
 Point & Arrow::GetPoint2()
{
    return point[1];
}
 Point & Arrow::GetPoint3()
{
    return point[2];
}
void Arrow::PrintArrowCoords()
{
    std::cout<<"CoordArrowPoint1:"<<std::endl;
    point[0].PrintCoords();
    std::cout<<"CoordArrowPoint2:"<<std::endl;
    point[1].PrintCoords();
    std::cout<<"CoordArrowPoint3"<<std::endl;
    point[2].PrintCoords();
}
void Arrow::DrawArrow()
{
if(flag==0)
    glColor3f(0.3,0.2,0.1);
else
      glColor3f(0,1,0);

    glBegin(GL_TRIANGLES);
    glVertex3f(point[0].x(),point[0].y(),point[0].z());
    glVertex3f(point[1].x(),point[1].y(),point[1].z());
    glVertex3f(point[2].x(),point[2].y(),point[2].z());
    glEnd();
}

void Arrow::SetArrowCoords(float x1, float y1, float z1, float x2, float y2, float z2,float x3, float y3, float z3 )
{
    point[0].PointSetCoords(x1,y1,z1);
    point[1].PointSetCoords(x2,y2,z2);
    point[2].PointSetCoords(x3,y3,z3);

}
bool Arrow::CheckMousePointer(float x, float y)
{
    double dist=sqrt(abs((x-middle_x)*(x-middle_x))+abs((y-middle_y)*(y-middle_y)));
    if(dist<40)
    {
        flag=1;
        return true;
    }
    else
    {
        flag=0;
        return false;
    }
}

void Arrow::ArrowRotate(float degree, float x, float y, float z)
{
       for(int i=0;i<3;i++)
           point[i].PointRotate(degree,x,y,z);
}

void Arrow::SetArrowCoords(float *coords)
{
    point[0].PointSetCoords(coords[0],coords[1],coords[2]);
    point[1].PointSetCoords(coords[3],coords[4],coords[5]);
    point[2].PointSetCoords(coords[6],coords[7],coords[8]);
}
void Arrow::SetMiddle(float x_middle,float y_middle)
{
    middle_x=x_middle;
    middle_y=y_middle;
}
