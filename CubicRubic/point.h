#ifndef POINT_H
#define POINT_H
#include <QMatrix4x4>
#include <QVector3D>
class Point
{
public:
    Point();
    Point(float x, float y, float z);
    Point(Point & ref);
    Point& operator=(Point & ref);
    void PointSetCoords(float x, float y, float z);
    float x();
    float y();
    float z();
    QVector4D  PointGetCoords();
    void PrintCoords();
    void PointRotate(float degree, float x, float y, float z);
    void PointTranslate(float _x, float _y, float _z);
    void SetMatrixIdentity();
private:
    QMatrix4x4 matrix4x4;
    QVector4D vector4d;
};

#endif // POINT_H


