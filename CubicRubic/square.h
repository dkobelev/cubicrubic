#ifndef SQUARE_H
#define SQUARE_H
#include "point.h"
#include <QMatrix4x4>
#include <QVector3D>
#include <QWidget>
#include <QGLWidget>
class Square : public Point
{
public:
    Square();
    Square(Point * point);
    Square(Square & sqr);
    Square & operator=(Square &  sqr);
    void SetSquareCoords(Point * point);
    Point * GetSquareCoords();
    Point & GetPoint1();
    Point & GetPoint2();
    Point & GetPoint3();
    Point & GetPoint4();
    void PrintSquareCoords();
    void DrawSquare();
    void SquareRotate(float degree, float x, float y, float z);
    void SquareTranslate(float _x, float _y, float _z);
    void SetSquareMatrixIdentity();
private:
    Point points[4];
};

#endif // SQUARE_H
