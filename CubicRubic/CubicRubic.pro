#-------------------------------------------------
#
# Project created by QtCreator 2014-01-31T22:49:29
#
#-------------------------------------------------

QT       += core gui opengl widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CubicRubic
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    drawwindow.cpp \
    cube.cpp \
    point.cpp \
    square.cpp \
    arrow.cpp \
    camera.cpp

HEADERS  += mainwindow.h \
    drawwindow.h \
    cube.h \
    point.h \
    square.h \
    arrow.h \
    camera.h

FORMS    += mainwindow.ui
