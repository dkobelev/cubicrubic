#include "cube.h"
#include <iostream>
#include <QGLWidget>
#include <QVector4D>

Cube::Cube()
{
    Point points[4];
    for (int i=0;i<4;i++)
    {
        points[i].PointSetCoords(0,0,0);
    }
        for(int i=0;i<6;i++)
    {
        square[i].SetSquareCoords(points);
    }
}

Cube::Cube(Square * sqr)
{
    for(int i=0;i<6;i++)
        square[i]=sqr[i];
}

Cube::Cube( Cube & cube)
{

    square[0]=cube.GetSquare1();
    square[1]=cube.GetSquare2();
    square[2]=cube.GetSquare3();
    square[3]=cube.GetSquare4();
    square[4]=cube.GetSquare5();
    square[5]=cube.GetSquare6();

}

Cube & Cube::operator=(Cube & cube)
{
//    for(int i=0; i<6;i++)
//     square[i]=cube.GetCubeCoords(i);
    square[0]=cube.GetSquare1();
    square[1]=cube.GetSquare2();
    square[2]=cube.GetSquare3();
    square[3]=cube.GetSquare4();
    square[4]=cube.GetSquare5();
    square[5]=cube.GetSquare6();
    return *this;
}

void Cube::SetCubeCoords(Square * sqr)
{
    for(int i=0;i<6;i++)
        square[i]=sqr[i];
}

Square & Cube::GetCubeCoords(int num)
{
return square[num];
}

Square & Cube::GetSquare1()
{
return square[0];
}

Square & Cube::GetSquare2()
{
return square[1];
}

Square & Cube::GetSquare3()
{
return square[2];
}

Square & Cube::GetSquare4()
{
return square[3];
}

Square & Cube::GetSquare5()
{

return square[4];
}

Square & Cube::GetSquare6()
{
return square[5];
}

void Cube::PrintCubeCoords()
{
for(int i=0;i<6;i++)
    square[i].PrintSquareCoords();
}

void Cube::DrawCube()
{
    glColor3f(0,1,0);
    square[0].DrawSquare();
    glColor3f(1,0,0);
    square[1].DrawSquare();
    glColor3f(1,1,1);
    square[2].DrawSquare();
    glColor3f(1,0.5,0);
    square[3].DrawSquare();
    glColor3f(1,1,0);
    square[4].DrawSquare();
    glColor3f(0,0,1);
    square[5].DrawSquare();

}

void Cube::CubeRotate(float degree, float x, float y, float z)
{
    for(int i=0;i<6;i++)
        square[i].SquareRotate(degree,x,y,z);
}

void Cube::CubeTranslate(float _x, float _y, float _z)
{
    for(int i=0;i<6;i++)
        square[i].SquareTranslate(_x,_y,_z);
}

void Cube::CleanMatrixPoints()
{
   for(int i=0;i<6;i++)
   {
         square[i].SetSquareMatrixIdentity();
   }
}
