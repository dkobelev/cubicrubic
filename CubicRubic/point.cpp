#include "point.h"
#include <iostream>
#include <QGLWidget>
#include <QVector4D>

Point::Point()
{
    matrix4x4.setToIdentity();
    vector4d.setW(1);
    vector4d.setX(0);
    vector4d.setY(0);
    vector4d.setZ(0);

}

Point::Point(float x, float y, float z)
{
    matrix4x4.setToIdentity();
    vector4d.setW(1);
    vector4d.setX(x);
    vector4d.setY(y);
    vector4d.setZ(z);
}
Point::Point(Point & ref)
{
    matrix4x4=ref.matrix4x4;
    vector4d=ref.vector4d;
}
Point& Point::operator = (Point & ref)
{
    matrix4x4=ref.matrix4x4;
    vector4d=ref.vector4d;
    return * this;
}
void Point::PointSetCoords(float x, float y, float z)
{
    matrix4x4.setToIdentity();
    vector4d.setW(1);
    vector4d.setX(x);
    vector4d.setY(y);
    vector4d.setZ(z);
}
QVector4D Point::PointGetCoords()
{
    return vector4d;
}
void Point::PrintCoords()
{
    float * array=new float[16];
    std::cout<<"Coordinates of the point: "<<std::endl;
    std::cout<<"x: "<<vector4d.x()<<" "<<"y: "<<vector4d.y()<<" "<<"z: "<<vector4d.z()<<std::endl;
    std::cout<<"Transformation matrix: "<<std::endl;
    matrix4x4.copyDataTo(array);
    std::cout<<array[0]<<" "<<array[1]<<" "<<array[2]<<" "<<array[3]<<std::endl;
    std::cout<<array[4]<<" "<<array[5]<<" "<<array[6]<<" "<<array[7]<<std::endl;
    std::cout<<array[8]<<" "<<array[9]<<" "<<array[10]<<" "<<array[11]<<std::endl;
    std::cout<<array[12]<<" "<<array[13]<<" "<<array[14]<<" "<<array[15]<<std::endl;


}
void Point::PointRotate(float degree, float x, float y, float z)
{
   QVector4D tmp;
   tmp=vector4d;
   matrix4x4.rotate(degree,x,y,z);
   vector4d=matrix4x4*tmp;
}
void Point::PointTranslate(float _x, float _y, float _z)
{
    QVector4D tmp;
    tmp=vector4d;
    matrix4x4.translate(_x,_y,_z);
    vector4d=matrix4x4*tmp;
}

float Point::x()
{
    return vector4d.x();
}

float Point::y()
{
     return vector4d.y();
}

float Point::z()
{
      return vector4d.z();
}

void Point::SetMatrixIdentity()
{
    matrix4x4.setToIdentity();
}
