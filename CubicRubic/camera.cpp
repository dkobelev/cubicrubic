#include "camera.h"
#include <iostream>
#include <math.h>

#define PI 3,1415926535897932384626433832795
Camera::Camera()
{
    angle_alfa=0;
    angle_beta=45;
    num=100;
    x=0;
    y=0;
    z=100;
    deltaX=0;
    deltaY=0;
    oldX=0;
    oldY=0;
    longitude=0;
    latitude=0;
    orintation_y=1;
}

int Camera::GetOrint()
{
        return orintation_y;
}

float Camera::GetX()
{
    return x;
}

float Camera::GetY()
{
    return y;
}

float Camera::GetZ()
{
    return z;
}

float Camera::GetAngleBeta()
{
    return angle_beta;
}

float Camera::GetAngleAlfa()
{
    return angle_alfa;
}

void Camera::SetX(float n_x)
{
    x=n_x;
}

void Camera::SetY(float n_y)
{
    y=n_y;
}

void Camera::SetZ(float n_z)
{
    z=n_z;
}

void Camera::SetAngleBeta(float a_x)
{
    angle_beta=a_x;
}

void Camera::SetAngleAlfa(float a_y)
{
    angle_alfa=a_y;
}

void Camera::setCamera(float alfa, float beta)
{
    deltaX =-(beta - oldX);
    deltaY =-(alfa - oldY);
//    std::cout<<" deltaX: "<< deltaX<<std::endl;
//    std::cout<<"deltaY: "<<deltaY<<std::endl;
   oldX = beta; oldY = alfa;

    longitude += deltaX*0.01;
    latitude += deltaY*0.01;


   // std::cout<<"latitude: "<<latitude<<std::endl;

    z=100*sin(latitude)*cos(longitude);
    x=100*sin(latitude)*sin(longitude);
    y=100*cos(latitude);

//    std::cout<<"X: "<<x<<std::endl;
//    std::cout<<"Y: "<<y<<std::endl;
//   std::cout<<"Z: "<<z<<std::endl;

    double angle_y_z=0.0;
    double angle_x_z=0.0;
 //   std::cout<<"tangence: "<<tangence<<std::endl;

    angle_y_z= (atan2f(y,z)*180)/PI;
     angle_x_z= (atan2f(x,z)*180)/PI;
//      std::cout<<"angle_y_z "<<angle_y_z<<std::endl;
//      std::cout<<" angle_x_z "<< angle_x_z<<std::endl;

 //   std::cout<<"orintation_y: "<<orintation_y<<std::endl;
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
//    if ( angle_y_z <90)
//                  {
                      orintation_y = 1;
//                  }
//                  else
//                  {
//                      orintation_y = -1;

//                  }
    gluLookAt(x, y, z,0,0,0, 0, orintation_y,0);

}

