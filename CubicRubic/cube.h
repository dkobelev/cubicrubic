#ifndef CUBE_H
#define CUBE_H
#include <QMatrix4x4>
#include <QVector3D>
#include "point.h"
#include "square.h"
class Cube:public Square
{
public:
    Cube();
    Cube(Square * sqr);
    Cube( Cube & cube);
    Cube & operator=(Cube & cube);
    void SetCubeCoords(Square * sqr);
    Square & GetCubeCoords(int num);
    Square & GetSquare1();
    Square & GetSquare2();
    Square & GetSquare3();
    Square & GetSquare4();
    Square & GetSquare5();
    Square & GetSquare6();
    void CleanMatrixPoints();
    void PrintCubeCoords();
    void DrawCube();
    void CubeRotate(float degree, float x, float y, float z);
    void CubeTranslate(float _x, float _y, float _z);
private:
    Square square[6];
};

#endif // CUBE_H

