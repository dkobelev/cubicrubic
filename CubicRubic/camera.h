#ifndef CAMERA_H
#define CAMERA_H
#include "drawwindow.h"
#include <GL/glu.h>
class Camera
{
public:
    Camera();
    float GetX();
    float GetY();
    float GetZ();
    float GetAngleBeta();
    float GetAngleAlfa();
    void SetX(float n_x);
    void SetY(float n_y);
    void SetZ(float n_z);
    void SetAngleBeta(float a_x);
    void SetAngleAlfa(float a_y);
    void setCamera(float alfa, float beta);
    int GetOrint();
private:
    float angle_beta;
    float angle_alfa;
    float num;
    float x;
    float y;
    float z;
    float deltaX;
    float deltaY;
    float oldX;
    float oldY;
    float longitude;
    float latitude;
    int orintation_y;
};

#endif // CAMERA_H
